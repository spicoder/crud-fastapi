from sqlalchemy import Column, Integer, String, Date
from database import Base
import bcrypt


# Define User class from Base
class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    username = Column(String(50), nullable=False)
    password_hash = Column(String(128), nullable=False)
    last_name = Column(String(50))
    first_name = Column(String(50))
    birthdate = Column(Date)
    email = Column(String(100), nullable=False, unique=True)

    def set_password(self, password):
        self.password_hash = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())

    def check_password(self, password):
        return bcrypt.checkpw(
            password.encode("utf-8"), self.password_hash.encode("utf-8")
        )
