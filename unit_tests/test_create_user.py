from http import client
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


def test_successful():
    # User data for a successful creation
    user_data = {
        "username": "john_doe",
        "password": "secretpassword",
        "last_name": "Doe",
        "first_name": "John",
        "birthdate": "1990-1-1",
        "email": "john.doe33@example.com",
    }

    response = client.post("/user", json=user_data)
    assert response.status_code == 201


def test_missing_nonrequire_fields():
    user_data = {
        "username": "john_doe",
        "password": "secretpassword",
        "last_name": "",
        "first_name": "",
        "birthdate": "",
        "email": "john.doe34@example.com",
    }

    response = client.post("/user", json=user_data)
    assert response.status_code == 201


def test_missing_require_fields():
    user_data = {
        "username": "john_doe",
        # Missing required fields: password, email
    }

    response = client.post("/user", json=user_data)
    assert response.status_code == 422
