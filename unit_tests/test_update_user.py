from http import client
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

valid_user_data = {
    "id": 1,
    "username": "carlo_doesnt",
    "last_name": "Doe",
    "first_name": "Carlo",
    "birthdate": "1990-01-01",
    "email": "john.doesnt@example.com",
}


def test_valid_update():
    # Positive test case: Valid user ID and complete user data for update
    response = client.put("/user/1", json=valid_user_data)
    assert response.status_code == 200
    updated_user_data = response.json()
    assert updated_user_data == valid_user_data


def test_invalid_user_id():
    # Negative test case: Invalid user ID (non-existing ID)
    response = client.put("/user/999", json=valid_user_data)
    assert response.status_code == 404


def test_invalid_input_data():
    # Negative test case: Invalid input data (missing required fields)
    invalid_data = {
        "username": "john_updated",
        "email": "john.updated@example.com"
        # Missing required fields: password, last_name, first_name, birthdate
    }
    response = client.put("/user/1", json=invalid_data)
    assert response.status_code == 422
