from http import client
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


def test_delete_existing_user():
    # Assuming user with id 1 exists in the database
    response = client.delete("/user/1")
    assert response.status_code == 204


def test_delete_non_existing_user():
    # Assuming user with id 1000 does not exist in the database
    response = client.delete("/user/1000")
    assert response.status_code == 404
    assert response.json() == {"detail": "user with id 1000 not found"}
