from http import client
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)

valid_user_data = {
    "id": 1,
    "username": "john_doe",
    "last_name": "Doe",
    "first_name": "John",
    "birthdate": "1990-01-01",
    "email": "john.doe33@example.com",
}


def test_get_user_by_id_valid_user():
    # Positive test case: Valid user ID should return 200 OK and correct user data
    response = client.get("/user/1")
    assert response.status_code == 200
    assert response.json() == valid_user_data


def test_get_user_by_id_invalid_user():
    # Negative test case: Invalid user ID should return 404 Not Found
    response = client.get("/user/999")
    assert response.status_code == 404


def test_get_user_by_id_non_integer_user_id():
    # Negative test case: Non-integer user ID should return 422 Unprocessable Entity
    response = client.get("/user/abc")
    assert response.status_code == 422
