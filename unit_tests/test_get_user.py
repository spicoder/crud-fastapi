from http import client
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


# Ensure response contains a list of users
def test_get_all_users():
    response = client.get("/user")
    assert response.status_code == 200
    assert len(response.json()) > 0


def test_get_all_users_invalid_endpoint():
    # Negative test case for invalid endpoint
    response = client.get("/users_invalid_endpoint")
    assert response.status_code == 404
