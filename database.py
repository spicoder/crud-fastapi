from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base

# Create a sqlite engine instance
DB_NAME = "fastapi.db"
engine = create_engine(f"sqlite:///{DB_NAME}")

# Create a DeclarativeMeta instance
Base = declarative_base()

# Create SessionLocal class from sessionmaker factory
SessionLocal = sessionmaker(bind=engine, expire_on_commit=False)
