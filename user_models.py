from typing import Optional
from pydantic import constr, BaseModel, ConfigDict, validator
from datetime import date

from sqlalchemy import true


# Pydantic model for creating a user
class UserCreate(BaseModel):
    username: constr(min_length=6, strict=True)
    password: constr(min_length=8, strict=True)
    last_name: str
    first_name: str
    birthdate: str
    email: constr(min_length=1, strict=True)


# Pydantic model for displaying a user
class User(BaseModel):
    ConfigDict(from_attributes=true)

    id: int
    username: str
    last_name: str = None
    first_name: str = None
    birthdate: date = None
    email: str


class UserUpdate(BaseModel):
    username: str
    last_name: str
    first_name: str
    birthdate: str
    email: str
