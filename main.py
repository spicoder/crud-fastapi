from fastapi import FastAPI, status, Depends, HTTPException
from database import Base, engine, SessionLocal
from sqlalchemy.orm import Session
import db_models
import user_models
from datetime import datetime
from typing import List
import bcrypt

# Create the database
Base.metadata.create_all(engine)

# Initialize the app
app = FastAPI()


# Helper function to get database session
def get_session():
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()


@app.get("/")
def root():
    return "Hello World!"


def validate_date(date_string):
    try:
        return datetime.strptime(date_string, "%Y-%m-%d").date()
    except ValueError:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Invalid date format. Please provide a date in the format YYYY-MM-DD.",
        )


@app.post("/user", status_code=status.HTTP_201_CREATED)
def create_user(user: user_models.UserCreate, session: Session = Depends(get_session)):
    # Hash the user's password before saving it to the database
    hashed_password = bcrypt.hashpw(user.password.encode("utf-8"), bcrypt.gensalt())

    user_obj = db_models.User(
        username=user.username,
        password_hash=hashed_password,  # Store the hashed password in the database
        last_name=user.last_name,
        first_name=user.first_name,
        birthdate=validate_date(user.birthdate) or datetime.today().date(),
        email=user.email,
    )

    session.add(user_obj)
    session.commit()
    session.refresh(user_obj)

    return user_obj


# GET ALL USERS
@app.get("/user", response_model=List[user_models.User])
def read_user_list(session: Session = Depends(get_session)):
    user_list = session.query(db_models.User).all()

    return user_list


# Get User by ID
@app.get("/user/{id}", response_model=user_models.User)
def read_user(id: int, session: Session = Depends(get_session)):
    # user_details = session.query(db_models.User).get(id)
    user_details = session.get(db_models.User, id)

    # check if id exists
    if not user_details:
        raise HTTPException(status_code=404, detail=f"user with id {id} not found")

    return user_details


# Update user
@app.put("/user/{id}", response_model=user_models.User)
def update_user(
    id: int,
    update_user: user_models.UserUpdate,
    session: Session = Depends(get_session),
):
    # Get the user by id
    user_details = session.get(db_models.User, id)

    if not user_details:
        raise HTTPException(status_code=404, detail=f"user with id {id} not found")

    # Validate sent data by user and update user details
    updated_fields = {
        "username": update_user.username,
        "last_name": update_user.last_name,
        "first_name": update_user.first_name,
        "birthdate": validate_date(update_user.birthdate) or datetime.today().date(),
        "email": update_user.email,
    }

    # Update user details
    for field, value in updated_fields.items():
        setattr(user_details, field, value)

    session.commit()

    return user_details


# Delete user
@app.delete("/user/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_user(id: int, session: Session = Depends(get_session)):
    user = session.get(db_models.User, id)

    if not user:
        raise HTTPException(status_code=404, detail=f"user with id {id} not found")

    try:
        session.delete(user)
        session.commit()
    except Exception as e:
        session.rollback()
        raise HTTPException(status_code=500, detail=f"Failed to delete user: {str(e)}")
